package com.company;

public class ForecastByHour {
	private final String date;
	private final double temperature;
	private final double humidity;
	private final double windSpeed;
	private final SideOfTheWorld sideOfTheWorld;

	public ForecastByHour(String date,double temperature, double humidity, double windSpeed, SideOfTheWorld sideOfTheWorld) {
		this.date = date;
		this.temperature = temperature;
		this.humidity = humidity;
		this.windSpeed = windSpeed;
		this.sideOfTheWorld = sideOfTheWorld;
	}

	public String getDate() {
		return date;
	}

	public double getHumidity() {
		return humidity;
	}

	public double getWindSpeed() {
		return windSpeed;
	}

	public double getTemperature() {
		return temperature;
	}

	@Override
	public String toString() {
		return "ForecastByHour{" +
				"date='" + date + '\'' +
				", temperature=" + temperature +
				", humidity=" + humidity +
				", windSpeed=" + windSpeed +
				", sideOfTheWorld=" + sideOfTheWorld +
				'}';
	}
}
