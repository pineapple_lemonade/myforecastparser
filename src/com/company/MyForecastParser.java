package com.company;

import java.io.*;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MyForecastParser {
	private final File file;

	private ArrayList<String> parseFile() {
		ArrayList<String> parsedFile = new ArrayList<>();
		try {
			FileReader fileReader = new FileReader(file);
			BufferedReader bufferedReader = new BufferedReader(fileReader);
			String string;
			while (true) {
				string = bufferedReader.readLine();
				if (string == null) {
					break;
				}
				parsedFile.add(string);
			}
			bufferedReader.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return parsedFile;
	}

	private String parseDate(String string) {
		String[] wholeDate = string.split("T");
		String hours = wholeDate[1].substring(0, 2) + ":" + wholeDate[1].substring(2, 4);
		String yearMonthDay = wholeDate[0].substring(0, 4) + "/" + wholeDate[0].substring(4, 6) + "/" + wholeDate[0].substring(6, 8);
		return yearMonthDay + " " + hours;
	}

	private double parseDouble(String number) {
		return Double.parseDouble(number);
	}

	private ArrayList<ForecastByHour> listOfForecastsBuilder(ArrayList<String> parsedFile) {
		ArrayList<ForecastByHour> listOfForecasts = new ArrayList<>();
		ArrayList<String> onlyUsefulStrings = deleteExcessStrings(parsedFile);
		for (int i = 0; i < onlyUsefulStrings.size(); i++) {
			String[] splitStringFromForecast = onlyUsefulStrings.get(i).split(",");
			String date = parseDate(splitStringFromForecast[0]);
			double temperature = parseDouble(splitStringFromForecast[1]);
			double humidity = parseDouble(splitStringFromForecast[2]);
			double windSpeed = parseDouble(splitStringFromForecast[3]);
			double angle = parseDouble(splitStringFromForecast[4]);
			listOfForecasts.add(i, new ForecastByHour(date, temperature, humidity, windSpeed, new SideOfTheWorld(angle)));
		}
		return listOfForecasts;
	}

	private ArrayList<String> deleteExcessStrings(ArrayList<String> parsedFile) {
		Pattern pattern = Pattern.compile("T\\d");
		int counter = -1;
		while (true) {
			counter++;
			Matcher matcher = pattern.matcher(parsedFile.get(counter));
			if (!matcher.find()) {
				parsedFile.set(counter, null);
			} else {
				break;
			}
		}
		parsedFile.removeIf(Objects::isNull);
		return parsedFile;
	}

	private String makeResultString(ArrayList<ForecastByHour> forecasts) {
		String result = "";
		String sideOfTheWorld = String.valueOf(whichSideTheMostFrequent());
		double averageTemp = averageSmth(forecasts, "temperature");
		double averageHumidity = averageSmth(forecasts, "humidity");
		double averageWindSpeed = averageSmth(forecasts, "windSpeed");
		String dateOfHottest = dateOfTheMostCharacteristic(forecasts, "temperature");
		String dateOfHumidiest = dateOfTheMostCharacteristic(forecasts, "humidity");
		String dateOfFastest = dateOfTheMostCharacteristic(forecasts, "windSpeed");
		result += "The most frequent side of the world is " + sideOfTheWorld + "\n"
				+ "average temperature, humidity and wind speed is: " + averageTemp + ", " + averageHumidity + ", " + averageWindSpeed + "." + "\n"
				+ "The date of most value of temperature one is: " + dateOfHottest + "\n"
				+ "The date of most value of humidity one is: " + dateOfHumidiest + "\n"
				+ "The date of most value of wind speed one is: " + dateOfFastest;
		return result;
	}

	private double averageSmth(ArrayList<ForecastByHour> forecastByHours, String field/* put here what field you need to find*/) {
		double average = 0;
		switch (field) {
			case "temperature":
				average = averageSmthIterator(forecastByHours, 1);
				break;
			case "humidity":
				average = averageSmthIterator(forecastByHours, 2);
				break;
			case "windSpeed":
				average = averageSmthIterator(forecastByHours, 3);
				break;
		}
		return average;
	}

	private String dateOfTheMostCharacteristic(ArrayList<ForecastByHour> forecastByHours, String field) {
		String date = "";
		switch (field) {
			case "temperature":
				forecastByHours.sort(Comparator.comparingDouble(ForecastByHour::getTemperature));
				date = forecastByHours.get(forecastByHours.size() - 1).getDate();
				break;
			case "humidity":
				forecastByHours.sort(Comparator.comparingDouble(ForecastByHour::getHumidity));
				date = forecastByHours.get(forecastByHours.size() - 1).getDate();
				break;
			case "windSpeed":
				forecastByHours.sort(Comparator.comparingDouble(ForecastByHour::getWindSpeed));
				date = forecastByHours.get(forecastByHours.size() - 1).getDate();
				break;
		}
		return date;
	}

	private double averageSmthIterator(ArrayList<ForecastByHour> forecastByHours, int field) {
		double sum = 0;
		for (ForecastByHour forecastByHour : forecastByHours) {
			switch (field) {
				case 1:
					sum += forecastByHour.getTemperature();
					break;
				case 2:
					sum += forecastByHour.getHumidity();
					break;
				case 3:
					sum += forecastByHour.getWindSpeed();
					break;
			}
		}
		return sum / forecastByHours.size();
	}

	private Enum<NameOfSideOfTheWorld> whichSideTheMostFrequent() {
		ArrayList<Enum<NameOfSideOfTheWorld>> names = new ArrayList<>();
		names.add(NameOfSideOfTheWorld.EAST);
		names.add(NameOfSideOfTheWorld.SOUTH);
		names.add(NameOfSideOfTheWorld.WEST);
		names.add(NameOfSideOfTheWorld.NORTH);
		names.add(NameOfSideOfTheWorld.NORTHEAST);
		names.add(NameOfSideOfTheWorld.NORTHWEST);
		names.add(NameOfSideOfTheWorld.SOUTHEAST);
		names.add(NameOfSideOfTheWorld.SOUTHWEST);
		names.sort((o1, o2) -> {
			NameOfSideOfTheWorld name1 = (NameOfSideOfTheWorld) o1;
			NameOfSideOfTheWorld name2 = (NameOfSideOfTheWorld) o2;
			return name1.getCounter() - name2.getCounter();
		});
		return names.get(names.size() - 1);
	}

	public File makeOutputFile( String absolutePath,String fileName/*enter your name of file*/) {
		String result = makeResultString(listOfForecastsBuilder(parseFile()));
		File file = new File(absolutePath + fileName + ".txt");
		try {
			FileWriter fileWriter = new FileWriter(file);
			BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
			bufferedWriter.write(result);
			bufferedWriter.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return file;
	}

	public MyForecastParser(File file) {
		this.file = file;
	}

}
