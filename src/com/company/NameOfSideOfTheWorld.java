package com.company;

public enum NameOfSideOfTheWorld {
	WEST, EAST, SOUTH, NORTH, NORTHEAST, NORTHWEST, SOUTHWEST, SOUTHEAST, NULL;
	private int counter;

	NameOfSideOfTheWorld() {
		this.counter = 0;
	}

	public void incCounter() {
		this.counter += 1;
	}

	public int getCounter() {
		return counter;
	}
}
