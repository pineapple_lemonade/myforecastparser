package com.company;

public class SideOfTheWorld {
	private final NameOfSideOfTheWorld nameOfSideOfTheWorld;
	final int angleOfOneSide = 45;

	public SideOfTheWorld(double angle) {
		this.nameOfSideOfTheWorld = defineSide(angle);
		nameOfSideOfTheWorld.incCounter();
	}

	private NameOfSideOfTheWorld defineSide(double angle) {
		NameOfSideOfTheWorld nameOfSideOfTheWorld = NameOfSideOfTheWorld.NULL;
		int wholePart = (int) angle / angleOfOneSide;
		double remainderPart = angle % angleOfOneSide;
		if (wholePart >= 8) {
			wholePart = wholePart % 8;
		}
		switch (wholePart) {
			case 0:
				nameOfSideOfTheWorld = whichMore(NameOfSideOfTheWorld.NORTHEAST, NameOfSideOfTheWorld.EAST, remainderPart);
				break;
			case 1:
				nameOfSideOfTheWorld = whichMore(NameOfSideOfTheWorld.NORTH, NameOfSideOfTheWorld.NORTHEAST, remainderPart);
				break;
			case 2:
				nameOfSideOfTheWorld = whichMore(NameOfSideOfTheWorld.NORTHWEST, NameOfSideOfTheWorld.NORTH, remainderPart);
				break;
			case 3:
				nameOfSideOfTheWorld = whichMore(NameOfSideOfTheWorld.WEST, NameOfSideOfTheWorld.NORTHWEST, remainderPart);
				break;
			case 4:
				nameOfSideOfTheWorld = whichMore(NameOfSideOfTheWorld.SOUTHWEST, NameOfSideOfTheWorld.NORTHEAST, remainderPart);
				break;
			case 5:
				nameOfSideOfTheWorld = whichMore(NameOfSideOfTheWorld.SOUTH, NameOfSideOfTheWorld.SOUTHWEST, remainderPart);
				break;
			case 6:
				nameOfSideOfTheWorld = whichMore(NameOfSideOfTheWorld.SOUTHEAST, NameOfSideOfTheWorld.SOUTH, remainderPart);
				break;
			case 7:
				nameOfSideOfTheWorld = whichMore(NameOfSideOfTheWorld.EAST, NameOfSideOfTheWorld.SOUTHEAST, remainderPart);
				break;
		}
		return nameOfSideOfTheWorld;
	}

	private NameOfSideOfTheWorld whichMore(NameOfSideOfTheWorld name1, NameOfSideOfTheWorld name2, double remainderPart) {
		if (remainderPart > (double) angleOfOneSide / 2) {
			return name1;
		}
		return name2;
	}

	@Override
	public String toString() {
		return "SideOfTheWorld{" +
				"nameOfSideOfTheWorld=" + nameOfSideOfTheWorld +
				'}';
	}
}
